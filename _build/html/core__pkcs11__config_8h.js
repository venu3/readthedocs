var core__pkcs11__config_8h =
[
    [ "configPKCS11_DEFAULT_USER_PIN", "core__pkcs11__config_8h.html#a5282c9ac3333552dabe3ee41214e668f", null ],
    [ "LIBRARY_LOG_LEVEL", "core__pkcs11__config_8h.html#ab09611739092a370c5d2e5a90a248c4c", null ],
    [ "LIBRARY_LOG_NAME", "core__pkcs11__config_8h.html#ac7330a40a68dc1f0aa6de997abbbb443", null ],
    [ "PKCS11_FREE", "core__pkcs11__config_8h.html#a49762c44ccfe43e9a0e4edf8208f66df", null ],
    [ "PKCS11_MALLOC", "core__pkcs11__config_8h.html#a62bc77bca58dbcdb95530cc20699f720", null ],
    [ "pkcs11configJITP_CODEVERIFY_ROOT_CERT_SUPPORTED", "core__pkcs11__config_8h.html#a1e1ba27edf13fe7a178d6396ae956fbc", null ],
    [ "pkcs11configLABEL_CODE_VERIFICATION_KEY", "core__pkcs11__config_8h.html#a16c1ed3b5df5f3bebb363f49ef29fc20", null ],
    [ "pkcs11configLABEL_DEVICE_CERTIFICATE_FOR_TLS", "core__pkcs11__config_8h.html#ad133b446f4dd937c7361ef008ebb5f28", null ],
    [ "pkcs11configLABEL_DEVICE_PRIVATE_KEY_FOR_TLS", "core__pkcs11__config_8h.html#a38f9707de35016d7d7c52a6113f0d0df", null ],
    [ "pkcs11configLABEL_DEVICE_PUBLIC_KEY_FOR_TLS", "core__pkcs11__config_8h.html#aabcf202a3a7ccad95b4388de2fccaee3", null ],
    [ "pkcs11configLABEL_JITP_CERTIFICATE", "core__pkcs11__config_8h.html#a6687a6580a35d8b23bbbc9b678126885", null ],
    [ "pkcs11configLABEL_ROOT_CERTIFICATE", "core__pkcs11__config_8h.html#a94fb7a945dcdcbd0311da3aaaf1d2e2c", null ],
    [ "pkcs11configMAX_LABEL_LENGTH", "core__pkcs11__config_8h.html#a8722be46b52a5dd4e5b320f9c16f4e00", null ],
    [ "pkcs11configMAX_NUM_OBJECTS", "core__pkcs11__config_8h.html#a579bd74b5a1b91411b472ae56eb94977", null ],
    [ "pkcs11configMAX_SESSIONS", "core__pkcs11__config_8h.html#ad0b85ca62291e1954b4b46fb80511428", null ],
    [ "pkcs11configOTA_SUPPORTED", "core__pkcs11__config_8h.html#af049ab760e0240985b1efbc44dae7811", null ],
    [ "pkcs11configPAL_DESTROY_SUPPORTED", "core__pkcs11__config_8h.html#a625350f68c74d81277747a80bb5b4141", null ],
    [ "pkcs11configSTORAGE_NS", "core__pkcs11__config_8h.html#a9a16815f267ecec20a35dd2bf3835f9f", null ],
    [ "pkcs11configSTORAGE_PARTITION", "core__pkcs11__config_8h.html#a86970642c68bf7fd56692ffdf950bbcd", null ]
];