var aws__ota__agent__config_8h =
[
    [ "configENABLED_CONTROL_PROTOCOL", "aws__ota__agent__config_8h.html#a20fed391548ba26a1bfd7fbfd362e6f3", null ],
    [ "configENABLED_DATA_PROTOCOLS", "aws__ota__agent__config_8h.html#afc4d1763ec6f9abaacdace650252e7cd", null ],
    [ "configOTA_PRIMARY_DATA_PROTOCOL", "aws__ota__agent__config_8h.html#a96092c27429db9fcabd02c0c6a15756a", null ],
    [ "otaconfigAGENT_PRIORITY", "aws__ota__agent__config_8h.html#a766d34dee20cbc36e1b440838f3adecb", null ],
    [ "otaconfigAllowDowngrade", "aws__ota__agent__config_8h.html#a95dac9f2da0f3bd64c2183caa6292efe", null ],
    [ "otaconfigFILE_REQUEST_WAIT_MS", "aws__ota__agent__config_8h.html#a19a42bab9dafd45b4c85709e55137534", null ],
    [ "otaconfigLOG2_FILE_BLOCK_SIZE", "aws__ota__agent__config_8h.html#a65facf4428a072324825abebb2b0e550", null ],
    [ "otaconfigMAX_NUM_BLOCKS_REQUEST", "aws__ota__agent__config_8h.html#aac052a7234601140e882f0075f196554", null ],
    [ "otaconfigMAX_NUM_OTA_DATA_BUFFERS", "aws__ota__agent__config_8h.html#a169ad4ac7245555d858f10d4df42be25", null ],
    [ "otaconfigMAX_NUM_REQUEST_MOMENTUM", "aws__ota__agent__config_8h.html#ae3ba450609211d1e00f1c540ca889fd8", null ],
    [ "otaconfigMAX_THINGNAME_LEN", "aws__ota__agent__config_8h.html#a5f9d4bbcc883e6ecff39f478a929df35", null ],
    [ "otaconfigSELF_TEST_RESPONSE_WAIT_MS", "aws__ota__agent__config_8h.html#a68e7932e806aed9025cb815b1cbf2104", null ],
    [ "otaconfigSTACK_SIZE", "aws__ota__agent__config_8h.html#a1c40af496bc98a8dd73eac8366e13317", null ]
];