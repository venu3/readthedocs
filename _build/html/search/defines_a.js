var searchData=
[
  ['otaconfigagent_5fpriority_648',['otaconfigAGENT_PRIORITY',['../aws__ota__agent__config_8h.html#a766d34dee20cbc36e1b440838f3adecb',1,'aws_ota_agent_config.h']]],
  ['otaconfigallowdowngrade_649',['otaconfigAllowDowngrade',['../aws__ota__agent__config_8h.html#a95dac9f2da0f3bd64c2183caa6292efe',1,'aws_ota_agent_config.h']]],
  ['otaconfigfile_5frequest_5fwait_5fms_650',['otaconfigFILE_REQUEST_WAIT_MS',['../aws__ota__agent__config_8h.html#a19a42bab9dafd45b4c85709e55137534',1,'aws_ota_agent_config.h']]],
  ['otaconfiglog2_5ffile_5fblock_5fsize_651',['otaconfigLOG2_FILE_BLOCK_SIZE',['../aws__ota__agent__config_8h.html#a65facf4428a072324825abebb2b0e550',1,'aws_ota_agent_config.h']]],
  ['otaconfigmax_5fnum_5fblocks_5frequest_652',['otaconfigMAX_NUM_BLOCKS_REQUEST',['../aws__ota__agent__config_8h.html#aac052a7234601140e882f0075f196554',1,'aws_ota_agent_config.h']]],
  ['otaconfigmax_5fnum_5fota_5fdata_5fbuffers_653',['otaconfigMAX_NUM_OTA_DATA_BUFFERS',['../aws__ota__agent__config_8h.html#a169ad4ac7245555d858f10d4df42be25',1,'aws_ota_agent_config.h']]],
  ['otaconfigmax_5fnum_5frequest_5fmomentum_654',['otaconfigMAX_NUM_REQUEST_MOMENTUM',['../aws__ota__agent__config_8h.html#ae3ba450609211d1e00f1c540ca889fd8',1,'aws_ota_agent_config.h']]],
  ['otaconfigmax_5fthingname_5flen_655',['otaconfigMAX_THINGNAME_LEN',['../aws__ota__agent__config_8h.html#a5f9d4bbcc883e6ecff39f478a929df35',1,'aws_ota_agent_config.h']]],
  ['otaconfigself_5ftest_5fresponse_5fwait_5fms_656',['otaconfigSELF_TEST_RESPONSE_WAIT_MS',['../aws__ota__agent__config_8h.html#a68e7932e806aed9025cb815b1cbf2104',1,'aws_ota_agent_config.h']]],
  ['otaconfigstack_5fsize_657',['otaconfigSTACK_SIZE',['../aws__ota__agent__config_8h.html#a1c40af496bc98a8dd73eac8366e13317',1,'aws_ota_agent_config.h']]]
];
